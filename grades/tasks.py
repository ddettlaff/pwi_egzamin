# -*- encoding: utf-8 -*-

from __future__ import absolute_import
from celery import shared_task
import requests
from bs4 import BeautifulSoup
import re
import zipfile
import StringIO

class InMemoryZip(object):
    def __init__(self):
        self.in_memory_zip = StringIO.StringIO()

    def append(self, filename_in_zip, file_contents):
        zf = zipfile.ZipFile(self.in_memory_zip, "a", zipfile.ZIP_DEFLATED, False)
        zf.writestr(filename_in_zip, file_contents)
        for zfile in zf.filelist:
            zfile.create_system = 0

        return self

    def read(self):
        self.in_memory_zip.seek(0)
        return self.in_memory_zip.read()


def courses(login, password):
    url = {
        'login': 'https://canvas.instructure.com/login',
        'grades': 'https://canvas.instructure.com/grades'
    }

    payload = {
        'pseudonym_session[unique_id]': login,
        'pseudonym_session[password]': password
    }

    session = requests.session()
    session.post(url["login"], data=payload)

    r = session.get(url["grades"])
    html = r.text
    bs = BeautifulSoup(html, "html5lib")

    table = bs.find('table', 'course_details')
    td = table.find_all('td', 'course')

    courses = {}
    for course in td:
        id_list = map(int, re.findall(r'\d+', course.a['href']))
        courses[course.text] = id_list[0] # wyciaga id kursu

    return courses

def grades(login, password, id):
    url = {
        'login': 'https://canvas.instructure.com/login',
        'grades': 'https://canvas.instructure.com/courses/' + str(id) + '/grades'
    }
    payload = {
        'pseudonym_session[unique_id]': login,
        'pseudonym_session[password]': password
    }
    session = requests.session()
    session.post(url["login"], data=payload)
    r = session.get(url["grades"])
    html = r.text
    bs = BeautifulSoup(html, "html5lib")

    grades = {}

    table = bs.find('table', id='grades_summary') # Tabela z ocenami
    trs = table.find_all('tr', 'student_assignment')

    for tr in trs[:-5]: # -5 bo podsumowanie w ostatnich
        grade = {}
        grade['name'] = tr.th.a.text.strip() # nazwa
        grade['date'] = tr.td.text.strip() # termin
        td = tr.find('td', 'assignment_score') # td z ocena
        grade['value'] = td.div.div.span.text.strip() # ocena

        category = tr.th.div.text.strip() # kategoria

        if grades.get(category) is not None:
            grades[category].append(grade)
        else:
            grades[category] = [grade, ]

    return grades


def string_output(grades): # grades = grades(...)
    string = ""
    for category in grades:
        string += category + "\r\n"
        for grade in grades[category]:
            string += grade['name'] + ' - ' + grade['date'] + ' - ' + grade['value'] + '\r\n'
        string += '\r\n'
    return string

@shared_task
def create_zip(login, password, id):
    string = string_output(grades(login, password, id))
    imz = InMemoryZip()
    imz.append("grades.txt", string.encode('utf-8'))
    return imz.read()