from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Canvas(models.Model):
    login = models.TextField(max_length=100)
    password = models.TextField(max_length=100)
    user = models.ForeignKey(User)