from django.shortcuts import render, redirect
from django.http import HttpResponse
#from django.contrib.auth.models import User
from grades.models import Canvas
from django.contrib.auth.decorators import login_required
import tasks
import requests
from django.utils import timezone
import datetime

# Create your views here.

@login_required
def index(request):
    canvas_objects = Canvas.objects.all()
    return render(request, 'grades/index.html', {'canvas_objects': canvas_objects})

@login_required
def login_info(request):
    if request.POST:
        if (request.POST.get('login') is not None) and (request.POST.get('password') is not None):
            try:
                canvas = Canvas.objects.get(user=request.user.id)
                canvas.login = request.POST.get('login')
                canvas.password = request.POST.get('password')
                canvas.save()
            except Canvas.DoesNotExist:
                canvas = Canvas(login=request.POST.get('login'), password=request.POST.get('password'), user=request.user)
                canvas.save()
            finally:
                return redirect('index')
        else:
            return redirect('login_info')

    return render(request, 'grades/login_info.html')

def courses_list(request):
    #canvas = Canvas.objects.get(user=request.user.id)
    #list = tasks.courses(canvas.login, canvas.password)
    if login_success(request.session.get('canvas_login'), request.session.get('canvas_password')):
        list = tasks.courses(request.session.get('canvas_login'), request.session.get('canvas_password'))
        return render(request, 'grades/courses_list.html', {'list': list})
    else:
        return render(request, 'grades/courses_list.html', {'list': {'Blad logowania': 1}})


def grades_zip(request, id):
    #canvas = Canvas.objects.get(user=request.user.id)
    #output = tasks.create_zip.delay(canvas.login, canvas.password, id)
    output = tasks.create_zip.delay(request.session.get('canvas_login'), request.session.get('canvas_password'), id)
    while output.result is None:
        pass
    response = HttpResponse(output.result, content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename=grades.zip'
    return response





def main_view(request):
    if request.POST:
        if login_success(request.POST.get('login'), request.POST.get('password')):
            request.session['canvas_login'] = request.POST.get('login')
            request.session['canvas_password'] = request.POST.get('password')
            request.session['start_date'] = timezone.now()
            #request.session.set_expiry(timezone.now() + datetime.timedelta(minutes=2))
            request.session.set_expiry(60*2)

            return render(request, 'nowe/main.html', {'success': 'Zalogowano', 'data': request.session['start_date']})
        else:
            return render(request, 'nowe/main.html', {'success': 'Bledne dane logowania'})


    return render(request, 'nowe/main.html', {'success': ''})


def login_success(login, password):
    url = {
        'login': 'https://canvas.instructure.com/login',
        'grades': 'https://canvas.instructure.com/grades'
    }

    payload = {
        'pseudonym_session[unique_id]': login,
        'pseudonym_session[password]': password
    }

    session = requests.session()
    session.post(url["login"], data=payload)

    r = session.get(url["grades"])
    html = r.text
    if r.url == 'https://canvas.instructure.com/login':
        return False
    else:
        return True


def session_info(request):
    if request.POST:
        request.session.set_expiry(60*2)
        request.session['start_date'] = timezone.now()

    if request.session.get('start_date') is not None:
        return render(request, 'nowe/session.html', {'czas': request.session.get_expiry_date()})
    else:
        request.session['start_date'] = timezone.now()
        #request.session.set_expiry(timezone.now() + datetime.timedelta(minutes=2))
        request.session.set_expiry(60*2)
        return render(request, 'nowe/session.html', {'czas': request.session.get_expiry_date()})
