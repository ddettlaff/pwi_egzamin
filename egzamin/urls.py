from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'egzamin.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),


    # grades app
    #url(r'^$', 'grades.views.index', name='index'),
    #url(r'^info/$', 'grades.views.login_info', name='login_info'),
    url(r'^courses/$', 'grades.views.courses_list', name='courses_list'),
    url(r'^grades/(?P<id>\d+)/$', 'grades.views.grades_zip', name='grades_zip'),


    # nowe
    url(r'^main/$', 'grades.views.main_view', name='main'),
    url(r'^sesja/$', 'grades.views.session_info', name='session_info'),


    # rejestracja
    #url(r'^accounts/', include('registration.backends.default.urls')),

    # admin
    url(r'^admin/', include(admin.site.urls)),
)
